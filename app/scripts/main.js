
var Mediator = (function () {

    var channels = {};

    //
    // Subscribe to a channel
    //
    function subscribe(channel, fn, that) {
        if (!channels[channel]) {
            channels[channel] = [];
        }
        channels[channel].push({
            context: that,
            callback: fn
        });
    }

    //
    // Publish a channel (like triggering an event)
    //
    function publish(channel) {

        if (!channels[channel]) {
            return false;
        }
        var args = Array.prototype.slice.call(arguments, 1);
        for (var i = 0, l = channels[channel].length; i < l; i += 1) {
            var subscription = channels[channel][i];
            if (typeof subscription.context === undefined) {
                subscription.callback(args);
            } else {
                subscription.callback.call(subscription.context, args);
            }
        }
    }

    return {
        subscribe: subscribe,
        publish: publish
    }

})();



var BackendConnector = (function () {
    function send() {
        $.ajax({
            url: "http://fettblog.eu/fh/numbers.php?jsonp",
            dataType: 'jsonp',
            jsonpCallback: 'callback',
            success: function (data) {
                Mediator.publish('datareceived', data.slots, data.result);
            },
            error: function (err) {

            }
        });
    }

    Mediator.subscribe('inputGiven', send);

})();

$(document).bind('keydown',function(e){
    var $coins = $('#coins');

    if(e.keyCode == 65) {

        if((parseInt($coins.text(), 10) > 0) && (!$('#third').data("animating"))){
            $coins.html(parseInt($coins.text(), 10) - 1);
            var $third = $('#third');
    	    var $first = $('#first');
    		var $second = $('#second');

           
    		$first.removeClass('slot-cherry').removeClass('slot-star').removeClass('slot-enemy').removeClass('slot-carrot');
    		$second.removeClass('slot-cherry').removeClass('slot-star').removeClass('slot-enemy').removeClass('slot-carrot');
    		$third.removeClass('slot-cherry').removeClass('slot-star').removeClass('slot-enemy').removeClass('slot-carrot');
            $third.data("animating", true);
            Mediator.publish('inputGiven');
        }

    }
});

$('#sound').on("ended", function() {
    if($('#third').data("animating")){
        $('#sound')[0].play();
    }

});


Mediator.subscribe('datareceived', function (result) {

    var $first = $('#first');
    var $second = $('#second');
    var $third = $('#third');
    var $lives = $('#lives');
    var $win = $('#win');
    var $lose = $('#lose');
    var $sound = $('#sound');

    $sound[0].play();



    if (result[0][0] == 0){
    	$first.addClass('slot-cherry');
    } else if (result[0][0] == 1) {
    	$first.addClass('slot-star');
    } else if (result[0][0] == 2) {
    	$first.addClass('slot-enemy');
    } else {
    	$first.addClass('slot-carrot');
    }
    if (result[0][1] == 0){
    	$second.addClass('slot-cherry');
    } else if (result[0][1] == 1) {
    	$second.addClass('slot-star');
    } else if (result[0][1] == 2) {
    	$second.addClass('slot-enemy');
    } else {
    	$second.addClass('slot-carrot');
    } 
    if (result[0][2] == 0){
    	$third.addClass('slot-cherry');
    } else if (result[0][2] == 1) {
    	$third.addClass('slot-star');
    } else if (result[0][2] == 2) {
    	$third.addClass('slot-enemy');
    } else {
    	$third.addClass('slot-carrot');
    } 
    var addAndNotAnimating = function() {
         $lives.html(parseInt($lives.text(), 10) + parseInt(result[1], 10));
         $(this).data("animating", false);
         if(parseInt(result[1], 10) > 0){
            $('#win')[0].play();
         }else{
            $('#lose')[0].play();
         }
    }
    $third.off().on("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", addAndNotAnimating);
});

